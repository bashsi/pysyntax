#!/usr/bin/python27
# -*- coding: utf-8 -*-
import time, filterutils
from filterutils import register_construction_filter

gyes = False
byes = False

def participle_turnover_filter(word,sentence):
	#print str(time.time())+' '+word.word
	waspres = False
	#if [((MAV.tag.POS=='PREP') or (MAV.tag.POS=='ADJF')) for MAV in word.morphs]:
	#		waspres = True
	for MAV in word.morphs:
		if (MAV.tag.POS=='PREP') or (MAV.tag.POS=='ADJF'):
			waspres = True
			break
	for MAV in word.morphs:
		if ({'GRND'} in MAV.tag) and (not waspres):
			word.morph = MAV
			#print MAV.word
			sentence.conq['participle_turnover'] = [word]
			hit = False
			for wrdo in reversed(sentence.words):
				if (wrdo==word):
					hit = True
				if not hit:
					continue
				if (not wrdo==word):
					if (wrdo.sym) or (wrdo.title):
						if (wrdo.sym):
							wrdo.sym = ''
							break
						if (wrdo.title):
							wrdo.participle_turnover = True
							sentence.conq['participle_turnover'].insert(0,wrdo)
							break
					else:
						wrdo.participle_turnover = True
						sentence.conq['participle_turnover'].insert(0,wrdo)
			sentence.gyes = True
			if ((word.sym=='.') or (word.sym==',')) and (not word.hgsym) and (sentence.gyes):
				word.participle_turnover = True
				sentence.conq['participle_turnover'].append(word)
				for wrdo in sentence.conq['participle_turnover']:
					wrdo.participle_turnover = True
				sentence.gyes = False
				ws = u"Деепричастный оборот: "
				for wrddata in sentence.conq['participle_turnover']:
					ws = ws + wrddata.word+wrddata.sym+' '
				if filterutils.debug:
					print ws
			break
		elif ((word.sym=='.') or (word.sym==',')) and (not word.hgsym) and (sentence.gyes):
			if 'participle_turnover' in sentence.conq:
				word.participle_turnover = True
				sentence.conq['participle_turnover'].append(word)
				for wrdo in sentence.conq['participle_turnover']:
					wrdo.participle_turnover = True
				sentence.gyes = False
				ws = u"Деепричастный оборот: "
				for wrddata in sentence.conq['participle_turnover']:
					ws = ws + wrddata.word+wrddata.sym+' '
				if filterutils.debug:
					print ws
			sentence.gyes = False
		elif sentence.gyes:
			if 'participle_turnover' in sentence.conq:
				word.participle_turnover = True
				sentence.conq['participle_turnover'].append(word)
			break

def participial_filter(word,sentence):
	#print str(time.time())+' '+word.word
	for MAV in word.morphs:
		if ({'PRTF'} in MAV.tag):
			hit = False
			found = False
			for word_object in sentence.words:
				if hit:
					for form in word_object.morphs:
						if form.tag.POS=='NOUN':
							if ((form.tag.gender==MAV.tag.gender) or (MAV.tag.gender==None)) and (form.tag.number==MAV.tag.number) and (form.tag.case==MAV.tag.case):
								found = True
								break
					if found:
						break
				else:
					if (word_object==word):
						hit = True
			if found:
				break
			word.morph = MAV
			word.participial = True
			sentence.conq['participial'] = [word]
			hit = False
			for wrdo in reversed(sentence.words):
				if (wrdo==word):
					hit = True
				if not hit:
					continue
				if (not wrdo==word):
					if (wrdo.sym) or (wrdo.title):
						if (wrdo.sym):
							wrdo.sym = ''
							break
						if (wrdo.title):
							wrdo.participial = True
							sentence.conq['participial'].insert(0,wrdo)
							break
					else:
						wrdo.participial = True
						sentence.conq['participial'].insert(0,wrdo)
			sentence.byes = True
			break
		elif ((word.sym=='.') or (word.sym==',')) and (not word.hgsym) and (sentence.byes):
			if 'participial' in sentence.conq:
				word.participial = True
				sentence.conq['participial'].append(word)
				sentence.byes = False
				ws = u"Причастный оборот: "
				for wrddata in sentence.conq['participial']:
					ws = ws + wrddata.word+wrddata.sym+' '
				if filterutils.debug:
					print ws
		elif sentence.byes:
			word.participial = True
			if 'participial' in sentence.conq:
				sentence.conq['participial'].append(word)
			break

register_construction_filter(participle_turnover_filter)
register_construction_filter(participial_filter)

# Число, род, падеж должны совпадать причастие -> существительное