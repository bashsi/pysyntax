#!/usr/bin/python27
# -*- coding: utf-8 -*-
import pymorphy2, filterutils
from filterutils import filters
morph = pymorphy2.MorphAnalyzer()

class Word:
	"""Word class for pysyntax"""
	morphs = []
	morph = None
	sym = ""
	title = False
	word = ""
	hgsym = False
	participial = False
	participle_turnover = False
	def __init__(self,wrd):
		if len(wrd)>0:
			wrd = wrd.replace('"','')
			if len(wrd)-1>=0:
				sym = wrd[len(wrd)-1]
			else:
				sym = ''
			if sym in [',','.','!','?',':']:
				wrd = wrd.replace(sym,'')
			else:
				sym = ''
			s = morph.parse(wrd)
			if len(wrd):
				self.title = wrd[0].istitle()
			self.sym = sym
			self.morphs = s
			self.word = wrd
			self.hgsym = False
			self.participial = False
			self.participle_turnover = False

class Sentence:
	"""Sentence class for pysyntax"""
	senline = ""
	words = []
	basis = []
	conq = {}
	history = {}
	gyes = False
	byes = False
	def __init__(self, line):
		# Cleaning all previous data
		self.senline = ""
		self.words = []
		self.basis = []
		self.conq = {}
		self.history = []
		self.gyes = False
		self.byes = False
		if line:
			self.senline = line
			self.parse(self.senline)

	def parse(self, toParse):
		wrds = toParse.split(" ") # Split line by space char
		for wrd in wrds:
			wobject = Word(wrd) # Creating new word object
			self.words.append(wobject) # Adding word object into words array
		for wrd in self.words:
			self.process_constructions(wrd) # Proccessing constructions in sentence

	def getWords(self):
		return len(self.words)

	def process_constructions(self,word):
		result = [] # Array of filter execution result
		for filter_func in filters:
			result.append(filter_func(word,self)) # Applying filter to  

	def dump(self):
		print self.senline # Printing out sentence (it will be printed out with some more data later)
		#for key in self.conq.keys():
		#	print 'Key: '+key
		#	print self.conq[key]

	def dump2(self):
		sen = ""
		cl = False
		broken = 0
		for wrd in self.words:
			if (not wrd.participial) and (not wrd.participle_turnover):
				word = wrd.word
				if (not cl):
					cl = True
					word = wrd.word.title()
				sen += word+wrd.sym+' '
			else:
				broken+=1
			#else:
			#	if wrd.participial:
			#		sen += wrd.word+'(part) '
			#	else:
			#		sen += wrd.word+'(turnover) '
		sen = sen[0:len(sen)-1]
		if (len(sen)>1) and (sen[len(sen)-1]==','):
			sen = sen[0:len(sen)-1]
			sen += '.'
		elif (len(sen)>1) and (not sen[len(sen)-1] in ['.','?','!']):
			sen += '.'
		print sen
		if filterutils.debug:
			print ""
		return broken