#!/usr/bin/python27
# -*- coding: utf-8 -*-
from __future__ import division
import sys, synlib
reload(sys)
sys.setdefaultencoding("utf-8")
# filters
import basis, homogeneous, part_turnover

allwords = 0
broken = 0
for line in open("result.txt","r"):
	sen = synlib.Sentence(line.strip().decode('utf-8'));
	allwords+=sen.getWords()
	#sen.dump()
	broken += sen.dump2()
print 'Stats: '+str(allwords)+' total, '+str(broken)+' removed ('+("{0:.1f}".format((broken)/(allwords)*100))+'%)'