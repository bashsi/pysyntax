#!/usr/bin/python27
# -*- coding: utf-8 -*-
from filterutils import register_construction_filter

def filter_basis(word,sentence):
	#Finding grammatic basis in sentence
	for MAV in word.morphs:
		if ({'NOUN','nomn'} in MAV.tag) or ({'NPRO','nomn'} in MAV.tag):
			word.morph = MAV
			sentence.history.append(word)
		elif {'VERB'} in MAV.tag:
			if sentence.history:
				for his in sentence.history:
					if (MAV.tag.number==his.morph.tag.number) and (((MAV.tag.gender==None) and (not his.morph.tag.POS=='NPRO')) or ((his.morph.tag.gender==None) and (his.morph.tag.POS=='NPRO') and (not MAV.tag.gender==None)) or ((MAV.tag.gender==his.morph.tag.gender) and (not his.morph.tag.gender==None))):
						#print u'Грамматическая основа: '+his.morph.word+' '+MAV.word
						vasdf = 123

register_construction_filter(filter_basis)