#!/usr/bin/python27
# -*- coding: utf-8 -*-
from filterutils import register_construction_filter

def homogeneous_filter(word,sentence):
	#blah
	was = {}
	for MAV in word.morphs:
		if MAV.tag.POS=='NOUN':
			# Trying to detect homogeneous members
			arrkey = 'homogen-'+str(MAV.tag.POS)+'-'+str(MAV.tag.case)
			if (arrkey in was) and (was[arrkey]):
				continue
			else:
				was[arrkey] = True
			if (arrkey in sentence.conq) and (sentence.conq[arrkey]):
				for ss in sentence.conq[arrkey]:
					if ss.sym:
						ss.hgsym = True
			else:
				sentence.conq[arrkey] = []
			word.morph = MAV
			word.homogen = True
			sentence.conq[arrkey].append(word)
			break
		elif MAV.tag.POS=='VERB':
			arrkey = 'homogen-'+str(MAV.tag.POS)+'-'+str(MAV.tag.tense)+'-'+str(MAV.tag.gender)
			if (arrkey in sentence.conq) and (sentence.conq[arrkey]):
				for ss in sentence.conq[arrkey]:
					if ss.sym:
						ss.hgsym = True
			else:
				sentence.conq[arrkey] = []
			word.morph = MAV
			word.homogen = True
			sentence.conq[arrkey].append(word)
		elif MAV.tag.POS=='ADJF':
			arrkey = 'homogen-'+str(MAV.tag.POS)+'-'+str(MAV.tag.case)+'-'+str(MAV.tag.gender)+'-'+str(MAV.tag.number)
			if (arrkey in sentence.conq) and (sentence.conq[arrkey]):
				for ss in sentence.conq[arrkey]:
					if ss.sym:
						ss.hgsym = True
			else:
				sentence.conq[arrkey] = []
			word.morph = MAV
			word.homogen = True
			sentence.conq[arrkey].append(word)

register_construction_filter(homogeneous_filter)